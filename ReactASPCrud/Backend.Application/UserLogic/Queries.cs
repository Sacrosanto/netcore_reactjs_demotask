﻿using Backend.Application.Interfaces;
using Backend.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Backend.Application.UserLogic
{
    public class GetAll : IRequest<List<Backend.Domain.User>>
    {
        
    }

    public class GetHandler : IRequestHandler<GetAll, List<Backend.Domain.User>>
    {
        public IApplicationDbContext _context;
        public GetHandler(IApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<List<Domain.User>> Handle(GetAll request, CancellationToken cancellationToken)
        {
            var users = _context.User.ToList();

            return users;
        }
    }


    public class GetById : IRequest<Backend.Domain.User>
    {
        public int UserId { get; set; }
    }

    public class GetById_Handler : IRequestHandler<GetById, Backend.Domain.User>
    {
        public IApplicationDbContext _context;
        public GetById_Handler(IApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<Domain.User> Handle(GetById request, CancellationToken cancellationToken)
        {
            var users = _context.User.SingleOrDefault(x => x.Id == request.UserId);

            return users;
        }

       
    }
}

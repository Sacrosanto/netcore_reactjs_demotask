﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Backend.Identity
{
    public static class UserRoles
    {
        public const string Admin = "Admin";
        public const string User = "User";
    }
}

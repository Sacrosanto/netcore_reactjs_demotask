using Backend.Application.UserLogic;
using Backend.Domain;
using MediatR;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactASPCrud.Services
{
    public class UserService : IUserService
    {
        IMediator _mediator;
        public UserService(IMediator mediator)
        {
            _mediator = mediator;

        }

        public async Task<List<User>> GetAll()
        {
            var users = await _mediator.Send(new Backend.Application.UserLogic.GetAll() { });
            return users;
        }

        public async Task<User> GetById(int id)
        {
            var users = await _mediator.Send(new Backend.Application.UserLogic.GetById() { UserId = id });
            return users;
        }

        public async Task<User> Create(User user)
        {
            var users = await _mediator.Send(new Backend.Application.UserLogic.UserCreate_RQ { User = user });
            return users;
        }

        public async Task<bool> Update(User user)
        {
            var users = await _mediator.Send(new Backend.Application.UserLogic.UserUpdate_RQ { User = user });
            return users;
        }

        public async Task<bool> Delete(int id)
        {
            var users = await _mediator.Send(new Backend.Application.UserLogic.UserDelete_RQ { UserId = id });
            return users;
        }
    }
}

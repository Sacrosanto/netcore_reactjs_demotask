﻿using Backend.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ReactASPCrud.Services
{
    public interface IUserService
    {
        Task<User> Create(User user);
        Task<bool> Delete(int id);
        Task<List<User>> GetAll();
        Task<User> GetById(int id);
        Task<bool> Update(User user);
    }
}
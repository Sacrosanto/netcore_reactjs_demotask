﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using Backend.Application.Interfaces;
using Backend.Domain;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Backend.Identity;

namespace Backend.Infraestructure.Persistence
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IApplicationDbContext
    {


        public DatabaseFacade Facade => this.Database;

        public DbSet<User> User { get; set; }

        public ApplicationDbContext() : base()
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

                optionsBuilder

                    .EnableSensitiveDataLogging(false)
                    .UseSqlite("Filename=KambdaTest.db",
                    b =>
                    {
                        b.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName);
                    });
            }



            base.OnConfiguring(optionsBuilder);
        }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext>  options) : base(options)
        {
            // _currentUserService = currentUserService;
            // _dateTime = dateTime;
        }
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            //foreach (var entry in ChangeTracker.Entries<AuditableEntity>())
            //{
            //    switch (entry.State)
            //    {
            //        case EntityState.Added:
            //            entry.Entity.CreatedBy = "";// _currentUserService.UserId;
            //            entry.Entity.Created = DateTime.Now;
            //            break;
            //        case EntityState.Modified:
            //            entry.Entity.LastModifiedBy = ""; //_currentUserService.UserId;
            //            entry.Entity.LastModified = DateTime.Now;
            //            break;
            //    }
            //}

            var response = await base.SaveChangesAsync(cancellationToken);

            return response;

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            base.OnModelCreating(builder);
        }
    }

}
